import React from 'react';
import GalleryItem  from './components/galleryItem';
import 'bootstrap/dist/css/bootstrap.min.css';
import './app.css';
import axios from 'axios';
import { Nav } from 'react-bootstrap';


//import './fonts/bootstrap/glyphicons-halflings-regular.eot';
//import './fonts/bootstrap/glyphicons-halflings-regular.svg';
//import './fonts/bootstrap/glyphicons-halflings-regular.ttf';
//import './fonts/bootstrap/glyphicons-halflings-regular.woff';
//import './fonts/bootstrap/glyphicons-halflings-regular.woff2';

class Profile extends React.Component {

  constructor(props) {
    super(props);

    //let userData = JSON.parse(localStorage.user);

    this.state = {
        popup:false,
        profile:{},
        account:{},
        posts:[],
        subscriptions:[]
    }
    
    this.subscribe = this.subscribe.bind(this);
    this.unsubscribe = this.unsubscribe.bind(this);
  }

  componentDidMount() {

    this.getProfile();
    this.getSubs();
    
    
  }

  getProfile() {
    const url = 'http://127.0.0.1:8000/api/profile/' + this.props.profile;
    axios.get(url)
    .then( response => response.data )
    .then( result =>  {
      
      this.setState({ profile:result, posts:result.posts})
    });
  }

  getSubs() {
    const urlSub = 'http://127.0.0.1:8000/api/subscriptions/' + localStorage.userId;
    
    axios.get(urlSub)
    .then( response => response.data )
    .then( result =>  {

      this.setState({ subscriptions: result})
    });
  }

  getPosts() {


    let posts = this.state.posts.map( post => <GalleryItem src={post.image} author={post.title} /> );
    return posts;
  }

  isFollowed() {
    let response = false;

    this.state.subscriptions.map((item) => {
      if (this.state.profile.id == item) {
        response = true;
      }
    });

    return response;
  }

  subscribe() {

    const urlSub = 'http://127.0.0.1:8000/api/subscribe';


    const data = new FormData();
    data.append('id', this.props.user.id);
    data.append('friend', this.state.profile.id);

    axios.post(urlSub, data)
    .then( response => response.data )
    .then( result =>  {
      this.getSubs();
      
    });
  
  }


  unsubscribe() {
    const urlSub = 'http://127.0.0.1:8000/api/unsubscribe';


    const data = new FormData();
    data.append('id', this.props.user.id);
    data.append('friend', this.state.profile.id);

    axios.post(urlSub, data)
    .then( response => response.data )
    .then( result =>  {
      this.getSubs();
      
    });
  }

  render() {
    
   const friends = this.isFollowed();
    let follow;

   if (localStorage.userId && (this.state.profile.id != this.props.user.id)) {
    follow = ((this.props.user.id != this.state.profile.id) && !friends) ? <div><Nav.Link onClick={this.subscribe} userId={this.state.profile.id} bsPrefix="btn btn-primary">Follow</Nav.Link></div> : <div><Nav.Link onClick={this.unsubscribe} bsPrefix="btn btn-warning">Unfollow</Nav.Link></div>;
   } else {
    follow ='';
   }
    

    return (
      <div id="profile">
        <div className="text-center">
          <h2>{this.state.profile.name}</h2>
          <img src={this.state.profile.profile_picture} />
          <div>
            <p>{this.state.profile.description}</p>
          </div>
          {follow}
        </div>
        <div className="row justify-content-around">
          {this.getPosts()}
        </div>
      </div>
    );
  }

}

export default Profile;
