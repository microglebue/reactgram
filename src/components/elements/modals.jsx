import React from 'react';
import LoginForm from '../forms/login';
import UploadForm from '../forms/upload';
import { Modal } from 'react-bootstrap';

class LoginModal extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal size="md" animation={false} show={this.props.show} onEscapeKeyDown={this.props.exit}>
                <Modal.Header>
                    <svg viewBox="0 0 366 86" height="40" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path d="M322.428 35.376h9.003v5.493h.183c1.12-3.66 4.231-6.039 8.122-6.039 4.23 0 7.1 2.196 7.91 6.068h.178c1.098-3.663 4.546-6.068 8.773-6.068 5.56 0 9.348 3.923 9.348 9.702v19.327h-9.27v-16.97c0-2.984-1.384-4.528-3.99-4.528-2.507 0-4.077 1.804-4.077 4.602v16.896h-8.85v-17.08c0-2.848-1.406-4.418-3.942-4.418-2.503 0-4.125 1.856-4.125 4.657v16.841h-9.263V35.376zm-4.491 28.483h-8.982v-5.39h-.18c-1.28 3.744-3.992 5.94-8.274 5.94-6.109 0-10.417-4-10.417-10.725V35.376h9.267v16.295c0 3.373 1.75 5.177 4.649 5.177 2.869 0 4.695-2.145 4.695-5.357V35.376h9.242V63.86zm-44.45-29.161c7.751 0 12.582 3.685 12.633 9.574h-8.458c-.077-2.043-1.694-3.322-4.256-3.322-2.27 0-3.73.993-3.73 2.453 0 1.129 1.04 1.943 3.18 2.387l5.955 1.199c5.666 1.202 8.093 3.53 8.093 7.85 0 5.907-5.351 9.698-13.29 9.698-8.297 0-13.103-3.791-13.494-9.621h8.981c.26 2.116 1.955 3.373 4.674 3.373 2.453 0 3.964-.89 3.964-2.405 0-1.155-.86-1.804-3.082-2.277l-5.376-1.126c-5.56-1.177-8.484-4.106-8.484-8.473 0-5.698 4.857-9.31 12.69-9.31zm-25.291 14.938c0-4.528-2.094-7.436-5.355-7.436-3.24 0-5.322 2.933-5.35 7.436.028 4.521 2.089 7.396 5.35 7.396 3.29 0 5.355-2.875 5.355-7.396zm9.446 0c0 9.28-4.15 14.645-11.225 14.645-4.176 0-7.31-1.991-8.696-5.288h-.176v14.073h-9.27v-37.69h9.215v5.079h.183c1.435-3.429 4.517-5.493 8.612-5.493 7.181 0 11.357 5.36 11.357 14.674zm-43.133 14.223h9.238V35.376h-9.238V63.86zm-.128-35.332c0-2.563 2.06-4.528 4.743-4.528 2.694 0 4.729 1.965 4.729 4.528 0 2.56-2.035 4.55-4.729 4.55-2.683 0-4.743-1.99-4.743-4.55zm-12.924 21.109c0-5.13-2.009-8.136-5.168-8.136-3.132 0-5.142 3.006-5.142 8.136 0 5.203 1.958 8.103 5.142 8.103 3.159 0 5.168-2.9 5.168-8.103zm-19.709-.03c0-9.309 5.717-14.908 14.541-14.908 8.876 0 14.542 5.57 14.542 14.909 0 9.522-5.59 14.93-14.542 14.93-8.956 0-14.54-5.437-14.54-14.93zm-12.839 0c0-4.55-2.064-7.406-5.355-7.406-3.312 0-5.321 2.827-5.321 7.407 0 4.601 1.983 7.164 5.321 7.164 3.32 0 5.355-2.563 5.355-7.164zm-19.398 16.108h8.952c.447 1.65 2.38 2.798 5.01 2.798 3.555 0 5.41-1.907 5.41-4.734V58.55h-.186c-1.124 3.006-4.465 5.074-8.638 5.074-6.79 0-11.302-5.177-11.302-14.098 0-9.1 4.304-14.564 11.434-14.564 4.125 0 7.283 2.248 8.56 5.463h.184v-5.049h9.186v28.171c0 6.798-6.002 11.033-14.848 11.033-8.096 0-13.239-3.685-13.762-8.866zm-12.869-16.078c0-5.13-2.009-8.136-5.167-8.136-3.133 0-5.143 3.006-5.143 8.136 0 5.203 1.958 8.103 5.143 8.103 3.158 0 5.167-2.9 5.167-8.103zm-19.709-.03c0-9.309 5.717-14.908 14.542-14.908 8.875 0 14.544 5.57 14.544 14.909 0 9.522-5.592 14.93-14.544 14.93-8.953 0-14.542-5.437-14.542-14.93zM103.926 63.86h9.267V26.122h-9.267v37.737z" fill="#44505B"></path><path fill="#D7E3F4" d="M14.64 70.813h46.79V23.938H14.64z"></path><path d="M14.64 70.813h46.79V23.938H14.64v46.874zM0 85.478h76.07V9.271H0V85.48z" fill="#007DFC"></path><path d="M78.258 28.605c0 11.752-9.508 21.278-21.238 21.278-11.727 0-21.232-9.526-21.232-21.278 0-11.748 9.505-21.274 21.232-21.274 11.73 0 21.238 9.526 21.238 21.274" fill="#D7E3F4"></path><path d="M57.024 14.665c-7.675 0-13.915 6.252-13.915 13.94 0 7.686 6.24 13.942 13.915 13.942 7.675 0 13.916-6.256 13.916-13.941 0-7.69-6.24-13.94-13.916-13.94m0 42.547c-15.745 0-28.555-12.833-28.555-28.607S41.279-.001 57.024-.001c15.746 0 28.556 12.833 28.556 28.607S72.77 57.213 57.024 57.213" fill="#007DFC"></path></g></svg>
                </Modal.Header>

                <Modal.Body>
                    <LoginForm exit={this.props.exit} />
                </Modal.Body>

                <Modal.Footer bsPrefix="modal-footer text-center">
                    &#169; Gabriel
                </Modal.Footer>
            </Modal>
        );
    }

}

class UploadModal extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal size="md" animation={false} show={this.props.show} onEscapeKeyDown={this.props.exit}>
                <Modal.Header>
                    <Modal.Title>
                        <i className="fa fa-upload"></i> Upload a file
                    </Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <UploadForm check={this.props.check} user={this.props.user} close={this.props.exit}  />
                </Modal.Body>
            </Modal>
        );
    }

}

export { LoginModal, UploadModal };