import React from 'react';
import { Card ,Col, Row, Nav} from 'react-bootstrap';

class Posts extends React.Component {
    
  constructor(props) {
      super(props);
  }

    render() {

        const user = this.props.user;

        return (
            <Row bsPrefix="row feeds">
                <Col lg="6">
                    <Card>
                        <Card.Img variant="top" src={this.props.image} />
                        <Card.Body>
                            <Card.Title>{this.props.title}</Card.Title>
                            <Card.Text>
                                {this.props.description}
                            </Card.Text>
                            <Card.Text>
                                <Nav.Link href={'profile/'+user.username}>@{user.name}</Nav.Link>
                            </Card.Text>
                            <Card.Text>
                                <i className="fa fa-heart"></i> <span>{this.props.likes}</span>
                            </Card.Text>
                        </Card.Body>
                    </Card>
            </Col>
          </Row>
        );
    }

}

export default Posts;