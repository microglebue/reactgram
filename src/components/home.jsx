import React from 'react';
import Post  from './posts';
import axios from 'axios';
import { Form, Button, InputGroup, ListGroup, Nav } from 'react-bootstrap';


class Home extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            posts:{},
            results:[]
        }

        this.search = this.search.bind(this);
        this.validate = this.validate.bind(this);
    }

    componentDidMount() {
        this.props.check();

       // Set in localstorage 
       const url = 'http://127.0.0.1:8000/api/newsfeed';
       const data = new FormData();
       data.append('id', localStorage.userId);
       axios.post(url, data)
       .then( response => response.data)
       .then( result =>  {
           this.setState({posts: result});
       });
    }

    search(event) {
      if (event.target.value.length > 0) {
        const url = 'http://127.0.0.1:8000/api/user/search';
        const data = new FormData();
        data.append('search', event.target.value);
        data.append('id', this.props.user.id);
        axios.post(url, data)
        .then( response => response.data)
        .then( result =>  {
            this.setState({results:result})
        });
      } else {
        this.setState({results:[]})
      }
    }

    validate(event) {

      if(!event.target.value.length && event.key == ' ')
      {
         return event.preventDefault();
      }
    }

    render() {

        let items = Object.values(this.state.posts).map((item)=>
            <Post key={item.id} title={item.title} image={item.image} description={item.description} likes={item.likes} user={item.user} />
        );

        let results = Object.values(this.state.results).map((result)=>
            <ListGroup.Item>
              <Nav.Link href={'/profile/'+result.username}>
                {result.name}
              </Nav.Link>
            </ListGroup.Item>
        );
        


        return(
          <div>
              <Form>
                <Form.Row>
                  <InputGroup>
                    <Form.Control className="col-8" placeholder="Username" aria-label="Username" aria-describedby="basic-addon2" name="email" onChange={this.search} onKeyPress={this.validate} />
                    <InputGroup.Append>
                      <Button variant="primary">Search</Button>
                    </InputGroup.Append>
                  </InputGroup>
                </Form.Row>
                <Form.Row>
                  <ListGroup className="col-8">
                      {results}
                  </ListGroup>
                </Form.Row>
              </Form>

            {items}
          </div>
          
        );
    /*
    const items = Object.values(this.state.images).map((item) => {
      return (<GalleryItem src={item.download_url} author={item.author} like={Math.floor((Math.random() * 999) + 1)} />)
    });

    return (
      <main>
        <Row>
          {items}
        </Row>
      </main>
    );
        */
    }

}

export default Home;