import React from 'react';
import { Form, Button } from 'react-bootstrap';
import axios from 'axios';

class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username:'',
            password:'',
            close: false
        }

        this.login = this.login.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
    }



    login() {

        // Set in localstorage 
        const url = 'http://127.0.0.1:8000/api/login/';
        const data = new FormData();
        Object.keys(this.state).map(field => data.append(field, this.state[field]));
        axios.post(url, data)
        .then( response => response.data)
        .then( result =>  {
            if (result !== '') {
                localStorage.userId = result;
                this.setState({close:true});
                this.props.exit();
            }
        });
    }

    changeHandler(event) {
        this.setState({[event.target.name] : event.target.value });
    }

    render() {
        return (
            <Form className="login-form">
                <Form.Row>
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" name="username" onChange={this.changeHandler} />
                </Form.Row>
                <Form.Row>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" name="password" onChange={this.changeHandler} />
                </Form.Row>
                <Form.Row bsPrefix="form-row btn-wrapper">
                    <Button variant="primary" onClick={this.login}>Login</Button>
                </Form.Row>
            </Form>
        );
    }

}

export default LoginForm;