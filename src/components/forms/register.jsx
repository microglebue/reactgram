import React from 'react';
import { Form, Button } from 'react-bootstrap';
import axios from 'axios';


class RegisterForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name:'',
            username:'',
            password:'',
            profile_picture:'',
            description:'',
        }

        this.register = this.register.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
    }

    changeHandler(event) {
        this.setState({[event.target.name] : (event.target.name == 'profile_picture') ? event.target.files[0] : event.target.value });
    }

    register() {
        const data = new FormData();
        Object.keys(this.state).map(field => data.append(field, this.state[field]));

        axios.post('http://127.0.0.1:8000/api/user/add', data)
       .then( response => {
            const url = 'http://127.0.0.1:8000/api/login/';
            const data = new FormData();
            Object.keys(this.state).map(field => data.append(field, this.state[field]));
            axios.post(url, data)
            .then( response => response.data)
            .then( result =>  {
                if (result !== '') {
                    localStorage.userId = result;
                    this.props.check();
                }
            });
       });
    }


    render() {
        return (
            <Form ref="registerForm" className="register-form" encType="multipart/form-data"> 
                <Form.Row>
                    <Form.Label>Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        name="name" 
                        onChange={this.changeHandler} />
                </Form.Row>
                <Form.Row>
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="text" name="username" onChange={this.changeHandler} />
                </Form.Row>
                <Form.Row>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" name="password" onChange={this.changeHandler} />
                </Form.Row>
                <Form.Row>
                    <Form.Label>Profile Picture</Form.Label>
                    <Form.Control type="file" name="profile_picture" onChange={this.changeHandler} />
                </Form.Row>
                <Form.Row>
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="text" name="description" onChange={this.changeHandler} />
                </Form.Row>
                <Form.Row bsPrefix="form-row btn-wrapper">
                    <Button variant="primary" onClick={this.register}>Register</Button>
                </Form.Row>
            </Form>
        );
    }

}

export default RegisterForm;