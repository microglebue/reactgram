import React from 'react';
import { Form, Button } from 'react-bootstrap';
import axios from 'axios';

class UploadForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title:'',
            description:'',
            image: ''
        }

        this.upload = this.upload.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
    }

    componentDidMount() {
        console.log(this.props);
        //.check();
    }

    changeHandler(event) {
        this.setState({[event.target.name] : (event.target.name == 'image') ? event.target.files[0] : event.target.value });
    }

    upload() {
        const data = new FormData();
        data.append('user_id', this.props.user.id)
        Object.keys(this.state).map(field => data.append(field, this.state[field]));

        axios.post('http://127.0.0.1:8000/api/post/add', data)
       .then( response => this.props.close() );
    }

    render() {
        return (
            <Form className="upload-form" encType="multipart/form-data">
                <Form.Row>
                    
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" name="title" onChange={this.changeHandler} />

                    <Form.Label>Caption</Form.Label>
                    <Form.Control as="textarea" name="description" rows="3" onChange={this.changeHandler} />

                    <Form.Label>Image</Form.Label>
                    <Form.Control type="file" name="image" accept=".png, .jpg, .jpeg" onChange={this.changeHandler} />
                </Form.Row>
                <Form.Row bsPrefix="form-row btn-wrapper upload">
                    <Button variant="primary" onClick={this.upload}>Upload</Button>
                </Form.Row>
            </Form>
        );
    }

}

export default UploadForm;