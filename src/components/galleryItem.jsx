import React from 'react';
import { Card ,Col} from 'react-bootstrap';

class GalleryItem extends React.Component {
    
  constructor(props) {
      super(props);
  }

    render() {

     

        return (
            <Col lg="4">
              <Card className="item images">
                <Card.Img variant="top" src={this.props.src} />
              </Card>
          </Col>
        );
    }

}

export default GalleryItem;