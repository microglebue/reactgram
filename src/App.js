import React from 'react';
import Header from './components/header';
import Home from './components/home';
import GalleryItem  from './components/galleryItem';
import { LoginModal, UploadModal } from './components/elements/modals';
import RegisterForm from './components/forms/register';
import { Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './app.css';
import Profile from './Profile';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import axios from 'axios';

//import './fonts/bootstrap/glyphicons-halflings-regular.eot';
//import './fonts/bootstrap/glyphicons-halflings-regular.svg';
//import './fonts/bootstrap/glyphicons-halflings-regular.ttf';
//import './fonts/bootstrap/glyphicons-halflings-regular.woff';
//import './fonts/bootstrap/glyphicons-halflings-regular.woff2';

class App extends React.Component {

  constructor(props) {
    super(props);

    //let userData = JSON.parse(localStorage.user);

    this.state = {
        popup:false,
        user : {},
        images:'',
    }

    this.logout     = this.logout.bind(this);
    this.openPopup  = this.openPopup.bind(this);
    this.exitPopup  = this.exitPopup.bind(this);
    this.checkUser  = this.checkUser.bind(this);
  }


  checkUser() {
    if (localStorage.userId) {

        const url = 'http://127.0.0.1:8000/api/user/';
        const data = new FormData();
        data.append('id', localStorage.userId);

        axios.post(url, data)
        .then( response => response.data)
        .then( result =>  {
            if (result !== '') {
              let user = this.state.user;
              user.id = result.id;
              user.name = result.name;
              user.username = result.username;
              user.image = result.profile_picture;
              user.description = result.description;
              user.isLoggedIn = result.isLoggedIn;
              this.setState({user});
            }
        });
    }
  }


  openPopup() {
    this.setState({popup:true}); 
  }

  exitPopup() {
    this.setState({popup:false}); 
  }

  logout() {
    // Set in localstorage 
    


    const url = 'http://127.0.0.1:8000/api/logout/';
    const data = new FormData();
    data.append('id', localStorage.userId);

    axios.post(url, data)
    .then( response => response.data)
    .then( result =>  {
      if (!result) {
        localStorage.removeItem("userId");
        this.setState({user:{}});
      }
    });


  }

  renderHeader() {
    return ;
  }

  renderLoginModal() {
    return <LoginModal show={this.state.popup} exit={this.exitPopup} check={this.checkUser} />;
  }

  renderUploadModal() {
    return <UploadModal show={this.state.popup} open={this.openPopup} exit={this.exitPopup} user={this.state.user} check={this.checkUser} />;
  }

  renderRegisterForm() {
    return (
      <Row>
        <Col lg="6">
          <div className="description">
            <h2>Title</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin risus velit, rhoncus vel accumsan malesuada, ornare sed arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed condimentum eu risus sed rutrum. Suspendisse quis congue est. Vestibulum pretium mattis mauris nec convallis. Etiam eget est sit amet dolor dignissim blandit in varius nisl. Phasellus in tellus finibus nunc dignissim luctus ut et diam.
            </p>
          </div>
        </Col>
        <Col lg="6">
          <h2>Register here:</h2>
          <RegisterForm  check={this.checkUser}/>
        </Col>
      </Row>
    );
  }

  renderMain() {
    return (<Home user={this.state.user} check={this.checkUser} />);
  }


  fetchData() {

  }


  render() {

    //console.log(JSON.(localStorage.user));

    let modal =  (localStorage.hasOwnProperty('userId')) ? this.renderUploadModal() : this.renderLoginModal();
    let content = (localStorage.hasOwnProperty('userId'))  ? this.renderMain() : this.renderRegisterForm();

    return (
      <Router>
        <div className="content">
          <div className="header-bg">
            <div className="container">
              <Header popup={this.openPopup} logout={this.logout} user={this.state.user} check={this.checkUser} />
            </div>
          </div>
          <div className="container">
            <main>
              <Switch>
                <Route path="/" exact>
                  {content}
                </Route>
                <Route path="/profile/:userId" render={ ({match}) =>
                  <Profile user={this.state.user} profile={match.params.userId} check={this.checkUser}  />
                } />
              </Switch>
            </main>
          </div>
          {modal}
        </div>
      </Router>
    );
  }

}

export default App;
