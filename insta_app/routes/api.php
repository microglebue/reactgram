<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/


Route::middleware('auth:api')->get('/user', function(Request $request){
    return $request->user();
});

Route::get('/profile/{username}','UserController@profile');
Route::get('/subscriptions/{id}','UserController@subscriptions');
Route::post('/newsfeed','UserController@newsfeed');
Route::post('/user/add', 'UserController@add');
Route::post('/login', 'UserController@login');
Route::post('/logout', 'UserController@logout');
Route::post('/user', 'UserController@user');
Route::post('/user/search', 'UserController@search');
Route::post('/subscribe', 'UserController@subscribe');
Route::post('/unsubscribe', 'UserController@unsubscribe');
Route::post('/post/add', 'PostsController@add');