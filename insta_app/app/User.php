<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $hidden = array('password','created_at','api_token','updated_at');

    //
    public function posts()
    {
        return $this->hasMany('App\Posts');
    }

    public function subscriptions()
    {
        return $this->hasOne('App\Subscriptions');
    }
}
