<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Posts;
use App\Subscriptions;
use Auth;

class UserController extends Controller
{
    //
    public function login(Request $request) {

        if ($request->isMethod('post')) {
            //
            $profile = User::where('username', $request->username)->first();
            //$pass = Hash::make($request->password);

            if ($profile && Hash::check($request->password, $profile->password)) {
                User::where('id', $profile->id)->update(['isLoggedIn' => 1]);
                return $profile->id;
            }
        }
    }

    public function logout(Request $request) {
        User::where('id', $request->id)->update(['isLoggedIn' => 0]);
        $profile = User::where('id', $request->id)->first();
        return $profile->isLoggedIn;
    }

    public function user(Request $request) {
        $profile = User::where('id', $request->id)->first();
        return response()->json($profile);
    }

    public function newsfeed(Request $request) {
        $profile = User::where('id', $request->id)->first();
        $friends = json_decode($profile->subscriptions->friends);

        $lists = User::whereIn('id', $friends)->get();
        foreach($lists as $list) {
            foreach($list->posts as $post) {
                $posts[$post->id] = $post;
                $posts[$post->id]['user'] = ['name' => $list->name, 'username' => $list->username];
            }
        }
        
        return response()->json($posts);
    }

    public function search(Request $request) {
        $profile = User::where('name','like', '%'.$request->search.'%')->where('id','!=',$request->id)->get();

        return response()->json($profile);
    }

    public function profile($username) {
        $profile = User::where('username', $username)->first();
        $profile->posts;
        $response = ($profile) ? $profile : 'not found';

        return response()->json($response);
    }

    public function subscribe(Request $request) {
        $subscriptions = Subscriptions::where('user_id', $request->id)->first();

        if ($subscriptions) {
            $friends = json_decode($subscriptions->friends);
            array_push($friends,$request->friend);
            return Subscriptions::where('user_id', $request->id)->update(['friends' => json_encode($friends)]);
        } else {
            $friends = [$request->friend];
            $id = Subscriptions::insertGetId(
                [
                    'friends'              => json_encode($friends),
                    'user_id'              => $request->id
                ]
            );
            return $id;
        }
    }

    public function unsubscribe(Request $request) {
        $subscriptions = Subscriptions::where('user_id', $request->id)->first();
        $friends = json_decode($subscriptions->friends);

        if (($key = array_search($request->friend, $friends)) !== false) {
            unset($friends[$key]);
        }

        return Subscriptions::where('user_id', $request->id)->update(['friends' => json_encode($friends)]);
    }
 

    public function subscriptions($id) {
        $profile = User::where('id', $id)->first();
        $profile->subscriptions;
        $response = ($profile) ? $profile : 'not found';

        return response()->json(json_decode($profile->subscriptions->friends));
    }
 
    
    public function add(Request $request) {

        $file = storage_path($request->file('profile_picture')->store('profile_pictures'));
        //$file =  Storage::disk('uploads')->put('test1', $request->file('profile_picture'));

        $id = User::insertGetId(
            [
                'name'              => $request->name,
                'username'          => $request->username,
                'password'          => Hash::make($request->password),
                'description'       => $request->description,
                'api_token'         => Hash::make($request->name),
                'profile_picture'   => $file,
                'isLoggedIn'        => 0
            ]
        );

        return $id;
    }
}
