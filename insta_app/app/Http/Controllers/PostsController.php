<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Posts;

class PostsController extends Controller
{
    public function add(Request $request) {

        $isLogged = $profile = User::where('id', $request->user_id)->first()->isLoggedIn;

        if ($isLogged) {

            $file = storage_path($request->file('image')->store('profile_pictures'));

            $id = Posts::insertGetId(
                [
                    'title'             => $request->title,
                    'description'       => $request->description,
                    'image'             => $file,
                    'user_id'           => $request->user_id,
                    'likes'             => 0
                ]
            );
    
            return $id;
        }
    }
}
